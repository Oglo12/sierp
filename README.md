# Sierp

Simple program that prints Sierpiński triangles in the terminal!

### Usage:

Simple run: `sierp`

Sierp also has flags for configuring color, height, width, etc...

For more options, run: `sierp --help`