mod cli;

use clap::Parser;
use owo_colors::OwoColorize;

fn main() {
    let args = cli::Cli::parse();

    let width = match args.width {
        Some(s) => s,
        None => (termsize::get().unwrap().cols / 2) as usize,
    };

    let height = match args.height {
        Some(s) => s,
        None => termsize::get().unwrap().rows as usize,
    };

    let mut bits: Vec<bool> = Vec::with_capacity(width);
    bits.resize_with(width, || { false });

    for _ in 0..height {
        let bit_iter: Vec<&bool> = match args.left {
            true => bits.iter().collect(),
            false => bits.iter().rev().collect(),
        };

        for i in bit_iter {
            print!("{}", match i {
                true => "██".color(args.color).to_string(),
                false => "  ".to_string(),
            });
        }

        print!("\n");

        advance_bits(&mut bits);

        std::thread::sleep(std::time::Duration::from_secs_f64(args.delay));
    }
}

fn advance_bits(bits: &mut Vec<bool>) {
    if bits.contains(&false) == false {
        for i in 0..bits.len() {
            bits[i] = false;
        }

        return;
    }

    let mut in_row: usize = 0;

    for i in bits.iter() {
        if *i {
            in_row += 1;
        }

        else {
            break;
        }
    }

    bits[in_row] = true;

    for i in 0..in_row {
        bits[i] = false;
    }
}
