use clap::Parser;
use owo_colors::AnsiColors;

#[derive(Parser)]
#[command(author, version, about, long_about = None)]
#[command(propagate_version = true)]
/// Generate Sierpiński triangle in the terminal
pub struct Cli {
    #[clap(short = 'W', long)]
    /// Specify a width (number of characters 2x because of the square made of double blocks)
    pub width: Option<usize>,

    #[clap(short = 'H', long)]
    /// Specify a height in characters
    pub height: Option<usize>,

    #[clap(short = 'C', long, default_value = "white")]
    /// Specify a built-in terminal color
    pub color: AnsiColors,

    #[clap(short, long)]
    /// Make the bits count from left to right
    pub left: bool,

    #[clap(short, long, default_value = "0.0")]
    /// Artificially slow down the program (in seconds)
    pub delay: f64,
}
